#ifndef TERMINATION_H
#define TERMINATION_H

#include "core.h"

// Everything behind global variables

bool TerminationHandler_setup(void);
void TerminationHandler_drop(void);

// Returns a file descriptor or `-1` on failure,
// close the descriptor to close the access
int TerminationHandler_access(void);

// Same as `wait_read` but is also responsible to
// notify every accesses of the termination
bool TerminationHandler_main_wait_read(int file);

// Waits until `file` is ready for read,
// Returns `false` if it was interrupted by termination
// or if an error occured
// THIS IS THE ONLY FUNCTION THAT CAN BE CALLED OUSIDE OF THE MAIN THREAD
bool TerminationHandler_wait_read(int file, int access);

#endif // TERMINATION_H
