#ifndef PERMITS_H
#define PERMITS_H

#include "core.h"

typedef struct {
    uint32_t count;
    uint32_t max;
    pthread_mutex_t mutex;
    pthread_cond_t on_release;
} Permits;

// Returns `true` and fills `p` on success,
// otherwise `p` is undefined
bool Permits_init(uint32_t max, Permits* p);
void Permits_drop(Permits* p);

// Acquire a permit, blocking if none is available
void Permits_acquire(Permits* p);

// Releases a permit, potentially unblocking an acquisition
void Permits_release(Permits* p);

// Waits for all the permits to be released
void Permits_wait_complete_release(Permits* p);

#endif // PERMITS_H
