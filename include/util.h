#ifndef UTIL_H
#define UTIL_H

#include "core.h"
#include "collections/vec.h"

int str_to_int(char* str, int min, int max);

typedef struct {
    char* mem;
    size_t pos;
    size_t len;
    size_t cap;
} IOBuffer;

#define IOBuffer_on_stack(name, capacity)       \
    char name##_mem[capacity];                  \
    IOBuffer name = (IOBuffer) {                \
        .mem = name##_mem,                      \
        .pos = 0,                               \
        .len = 0,                               \
        .cap = capacity                         \
    }

// Reads as much as possible from `file` into `buffer`,
// Returns `false` if nothing was read
bool IOBuffer_load(IOBuffer* buffer, int file);

// Writes all there is in `buffer` to `file`,
// Returns `false` on failure
bool IOBuffer_unload(IOBuffer* buffer, int file);

// Reads a byte from `buffer`, reading from `file` if necessary,
// Returns `false` if nothing was read
bool IOBuffer_read(IOBuffer* buffer, int file, char* byte);

// Writes a byte to `buffer`, writing into `file` if necessary,
// Returns `false` on failure
bool IOBuffer_write(IOBuffer* buffer, int file, char byte);

// Ensures that what `buffer` holds is written into `file`,
// Returns `false` on failure
bool IOBuffer_flush(IOBuffer* buffer, int file);

// Forgets everything that `buffer` holds
void IOBuffer_clear(IOBuffer* buffer);

// Writes a string to `buffer`, writing into `file` if necessary,
// Returns `false` on failure
bool IOBuffer_write_str(IOBuffer* buffer, int file, const char* str);

bool read_line(int file, IOBuffer* buffer, Vec* line);

const char* retrieve_extension(const char* path);

size_t whitespace_scan(const char* s, size_t len, size_t* begin, size_t* end);

// Returns a pointer into `line` or NULL
char* retrieve_mime_type(const char* extension, Vec* line);

#endif
