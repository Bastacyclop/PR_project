#ifndef DENIAL_H
#define DENIAL_H

#include "core.h"
#include "collections/vec.h"

typedef struct {
    pthread_mutex_t mutex;
    size_t threshold;
    Vec entries;
} DenialWatcher;

// Returns `true` and fills `watcher` on success,
// otherwise `watcher` is undefined
bool DenialWatcher_init(DenialWatcher* watcher, size_t threshold);
void DenialWatcher_drop(DenialWatcher* watcher);

bool DenialWatcher_check(DenialWatcher* watcher,
                         uint32_t address,
                         time_t time);

void DenialWatcher_report(DenialWatcher* watcher,
                          uint32_t address,
                          time_t time,
                          size_t content_length);

// Should run in a thread and be stopped with `pthread_cancel`
void DenialWatcher_garbage_collect(DenialWatcher* watcher);

#endif // DENIAL_H
