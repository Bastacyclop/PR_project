#ifndef LOG_H
#define LOG_H

#include "core.h"

typedef struct {
    FILE* file;
    pthread_mutex_t mutex;
} Log;

// Returns `true` and fills `log` on success,
// otherwise `log` is undefined
bool Log_init(const char* path, Log* log);
void Log_drop(Log* log);

typedef struct {
    const char* address;
    uint16_t port;
    time_t time;
    const char* request;
    const char* code;
    size_t content_length;
} LogEntry;

void Log_append(Log* log, const LogEntry* entry);
   
#endif // LOG_H
