#ifndef HTTP_H
#define HTTP_H

#include "core.h"
#include "util.h"
#include "log.h"
#include "denial.h"
#include "termination.h"

#include <netinet/in.h>

typedef struct {
    int file;
    struct sockaddr_in addr;
} Socket;

// Returns `true` and fills `s` on success,
// otherwise `s` is undefined
bool listen_to(int port, int queue_size, Socket* s);

// `s` becomes undefined
void close_listener(Socket* s);

// Returns `true` and fills `connection` on success,
// otherwise `connection` is undefined
bool wait_connection(const Socket* socket, Socket* connection);

// `connection` becomes undefined
void close_connection(Socket* connection);

void handle_connection(const Socket* connection,
                       Log* log,
                       DenialWatcher* watcher,
                       int termination);

#endif
