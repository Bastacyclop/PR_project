#include "log.h"

bool Log_init(const char* path, Log* log) {
    if ((log->file = fopen(path, "w")) == NULL) {
        perror("could not create log file\n");
        return false;
    }
    if (pthread_mutex_init(&log->mutex, NULL) != 0) {
        fprintf(stderr, "could not initialize mutex\n");
        fclose(log->file);
        return false;
    }
    return true;
}

void Log_drop(Log* log) {
    fclose(log->file);
    if (pthread_mutex_destroy(&log->mutex) != 0) {
        fprintf(stderr, "could not destroy mutex\n");
    }
}

void Log_append(Log* log, const LogEntry* entry) {
    const size_t DATE_BUF_SIZE = 32;
    char date_buf[DATE_BUF_SIZE];
    const char* date = date_buf;
    struct tm tm;
    if (localtime_r(&entry->time, &tm) == NULL) {
        perror("localtime_r");
        date = "unknown";
    } else if (strftime(date_buf, DATE_BUF_SIZE, "%F@%T", &tm) == 0) {
        fprintf(stderr, "could not format time\n");
        date = "unknown";
    }

    pthread_mutex_lock(&log->mutex);
    fprintf(log->file, "%s:%u %s %i %lu %s %s %lu\n",
            entry->address, entry->port, date, getpid(), pthread_self(),
            entry->request, entry->code, entry->content_length);
    pthread_mutex_unlock(&log->mutex);
}
