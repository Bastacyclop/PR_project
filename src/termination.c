#include "termination.h"
#include "collections/vec.h"

#include <fcntl.h>
#include <errno.h>
#include <poll.h>

static
void termination_sigh(int sig);
static
bool non_blocking_pipe_init(int files[2]);

static int main_pipe[2];
static Vec pipe_writes;

bool TerminationHandler_setup() {
    if (!non_blocking_pipe_init(main_pipe)) {
        return false;
    }

    sigset_t mask;
    sigemptyset(&mask);
    struct sigaction action = (struct sigaction) {
        .sa_handler = termination_sigh,
        .sa_mask = mask,
        .sa_flags = 0,
    };
    if (sigaction(SIGINT, &action, NULL) != 0) {
        fprintf(stderr, "could not setup termination signal action\n");

        close(main_pipe[0]);
        close(main_pipe[1]);
        return false;
    }

    pipe_writes = Vec_with_capacity(8, sizeof(int));
    return true;
}

void TerminationHandler_drop() {
    for (size_t i = 0; i < Vec_len(&pipe_writes); i++) {
        close(*(const int*)Vec_get(&pipe_writes, i));
    }
    Vec_drop(&pipe_writes);
    // let them be closed by the system because our signal handler
    // might still use them while we shut down,
    // we could also remove the signal handler.
    // close(main_pipe[0]);
    // close(main_pipe[1]);
}

int TerminationHandler_access() {
    int pipe_files[2];
    if (!non_blocking_pipe_init(pipe_files)) {
        return -1;
    }
    Vec_push(&pipe_writes, &pipe_files[1]);
    return pipe_files[0];
}

bool TerminationHandler_main_wait_read(int file) {
    if (TerminationHandler_wait_read(file, main_pipe[0])) {
        return true;
    } else {
        size_t i = 0;
        while (i < Vec_len(&pipe_writes)) {
            int w = *(const int*)Vec_get(&pipe_writes, i);
            if (write(w, "x", 1) < 0) {
                Vec_swap_remove(&pipe_writes, i, NULL);
                close(w);
            } else {
                i++;
            }
        }
        return false;
    }
}

bool TerminationHandler_wait_read(int file, int access) {
    struct pollfd pfs[2] = {
        { .fd = file, .events = POLLIN, .revents = 0 },
        { .fd = access, .events = POLLIN, .revents = 0 },
    };
    if (poll(pfs, 2, -1) < 0) {
        perror("could not poll");
        return false;
    }
    short termination_event = pfs[1].revents;
    if (termination_event) {
        if (termination_event & POLLIN) {
            char buf[128];
            while (read(access, buf, 128) > 0) {}
        }
        return false;
    }
    short file_event = pfs[0].revents;
    if (file_event & (POLLERR | POLLHUP | POLLNVAL)) {
        return false;
    }
    return true;
}

void termination_sigh(int sig) {
    (void)(sig);
    int errno_backup = errno;
    if (write(main_pipe[1], "x", 1) < 0) {
        // FIXME: not signal-safe
        perror("could not write to termination pipe");
        _exit(EXIT_FAILURE);
    }
    errno = errno_backup;
}

bool non_blocking_pipe_init(int files[2]) {
    if (pipe(files) < 0) {
        perror("could not create pipe");
        return false;
    }

    int flags;
    if ((flags = fcntl(files[0], F_GETFL) < 0) ||
        fcntl(files[0], F_SETFL, flags | O_NONBLOCK) < 0 ||
        (flags = fcntl(files[1], F_GETFL) < 0) ||
        fcntl(files[1], F_SETFL, flags | O_NONBLOCK))
    {
        perror("could not modify file status flags");
        close(files[0]);
        close(files[1]);
        return false;
    }

    return true;
}
