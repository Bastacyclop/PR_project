#include "core.h"
#include "util.h"
#include "http.h"
#include "permits.h"
#include "log.h"
#include "denial.h"
#include "termination.h"

typedef struct {
    Socket socket;
    int termination;
} Connection;

static
void* start_connection(void* connection);
static
void* start_collection(void* none);

Permits connection_permits;
Log log_file;
DenialWatcher denial_watcher;

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("usage: %s port [max_clients=8 [n=0]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    int port = str_to_int(argv[1], 0, INT_MAX);
    int max_clients = 8;
    int denial_threshold = 10000;

    if (argc > 2) {
        max_clients = str_to_int(argv[2], 0, INT_MAX);

        if (argc > 3) {
            denial_threshold = str_to_int(argv[3], 0, INT_MAX);

            if (argc > 4) {
                fprintf(stderr, "some arguments were ignored\n");
            }
        }
    }

    if (!Log_init("/tmp/http3402737-3402667.log", &log_file)) {
        return EXIT_FAILURE;
    }

    if (!Permits_init(max_clients, &connection_permits)) {
        Log_drop(&log_file);
        return EXIT_FAILURE;
    }

    if (!DenialWatcher_init(&denial_watcher, denial_threshold)) {
        Permits_drop(&connection_permits);
        Log_drop(&log_file);
        return EXIT_FAILURE;
    }

    if (!TerminationHandler_setup()) {
        DenialWatcher_drop(&denial_watcher);
        Permits_drop(&connection_permits);
        Log_drop(&log_file);
        return EXIT_FAILURE;
    }

    Socket socket;
    if (!listen_to(port, max_clients, &socket)) {
        TerminationHandler_drop();
        DenialWatcher_drop(&denial_watcher);
        Permits_drop(&connection_permits);
        Log_drop(&log_file);
        return EXIT_FAILURE;
    }

    printf("-- listening on port %i\n", port);

    pthread_attr_t thread_attr;
    if ((pthread_attr_init(&thread_attr) != 0) ||
        (pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_DETACHED) != 0)) {
        fprintf(stderr, "could not initialize thread attributes\n");

        close_listener(&socket);
        TerminationHandler_drop();
        DenialWatcher_drop(&denial_watcher);
        Permits_drop(&connection_permits);
        Log_drop(&log_file);
        return EXIT_FAILURE;
    }

    pthread_t collection_thread;
    if (pthread_create(&collection_thread, NULL, start_collection, NULL) != 0) {
        fprintf(stderr, "could not create thread\n");

        close_listener(&socket);
        TerminationHandler_drop();
        DenialWatcher_drop(&denial_watcher);
        Permits_drop(&connection_permits);
        Log_drop(&log_file);
        return EXIT_FAILURE;
    }

    while (TerminationHandler_main_wait_read(socket.file)) {
        Permits_acquire(&connection_permits);

        Connection* c = malloc(sizeof(Connection));
        assert_alloc(c);

        bool connection_failed = false;
        if (wait_connection(&socket, &c->socket)) {
            c->termination = TerminationHandler_access();
            if (c->termination < 0) {
                close_connection(&c->socket);
                connection_failed = true;
            } else {
                pthread_t t;
                if (pthread_create(&t, &thread_attr, start_connection, c) != 0) {
                    fprintf(stderr, "could not create thread\n");
                    close(c->termination);
                    close_connection(&c->socket);
                    connection_failed = true;
                }
            }
        } else {
            connection_failed = true;
        }

        if (connection_failed) {
            fprintf(stderr, "could not handle connection\n");
            free(c);
            Permits_release(&connection_permits);
        }
    }

    printf("-- server shutting down\n");
    pthread_cancel(collection_thread);
    pthread_join(collection_thread, NULL);
    Permits_wait_complete_release(&connection_permits);

    if (pthread_attr_destroy(&thread_attr) != 0) {
        fprintf(stderr, "could not destroy thread attributes\n");
    }
    close_listener(&socket);
    TerminationHandler_drop();
    DenialWatcher_drop(&denial_watcher);
    Permits_drop(&connection_permits);
    Log_drop(&log_file);

    printf("-- server shut down\n");
    // socket may be in `TIME_WAIT` state for a few minutes
    return EXIT_SUCCESS;
}

void* start_connection(void* connection) {
    Connection* c = connection;
    printf("-- connection started\n");
    handle_connection(&c->socket, &log_file, &denial_watcher, c->termination);
    close(c->termination);
    close_connection(&c->socket);
    printf("-- connection ended\n");
    free(c);
    Permits_release(&connection_permits);
    return NULL;
}

void* start_collection(void* none) {
    (void)(none);
    DenialWatcher_garbage_collect(&denial_watcher);
    return NULL;
}

