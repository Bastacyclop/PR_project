#include "http.h"
#include "util.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <semaphore.h>

typedef struct {
    char path[128];
    char* header; // dynamically allocated
} GetRequest;

typedef struct {
    const Socket* connection;
    Log* log;
    DenialWatcher* watcher;
    GetRequest request;
    LogEntry entry;
    sem_t can_write;
    sem_t* next_can_write;
} RequestArgs;

static
bool wait_get_request(int in_file, GetRequest* get);
static
bool receive_line(int file, IOBuffer* buffer, Vec* string);
static
void* answer_get_request(void* arg);
static
bool send_line(int file, IOBuffer* buffer, const char* line);
static
void send_content(RequestArgs* args,
                  IOBuffer* buffer,
                  int content_file);
static
int execute_out_file(const char* executable);
static
void ignore_sigh(int sig);

static const unsigned int EXECUTION_TIMEOUT_SECONDS = 10;

bool listen_to(int port, int queue_size, Socket* s) {
    s->file = socket(AF_INET, SOCK_STREAM, 0);
    if (s->file < 0) {
        perror("could not create socket");
        return false;
    }

    s->addr = (struct sockaddr_in) {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        .sin_addr = (struct in_addr) {
            .s_addr = INADDR_ANY
        },
        .sin_zero = { 0 }
    };

    if (bind(s->file, (struct sockaddr*)(&s->addr), sizeof(s->addr)) < 0) {
        perror("could not bind socket");
        close(s->file);
        return false;
    }

    if (listen(s->file, queue_size) < 0) {
        perror("could not listen to socket");
        close(s->file);
        return false;
    }

    return true;
}

void close_listener(Socket* s) {
    close(s->file);
}

bool wait_connection(const Socket* s, Socket* connection) {
    socklen_t connection_addr_len = sizeof(connection->addr);
    connection->file = accept(s->file,
                              (struct sockaddr*)(&connection->addr),
                              &connection_addr_len);
    assert(connection_addr_len == sizeof(connection->addr));
    if (connection->file < 0) {
        perror("could not accept connection");
        return false;
    }

    return true;
}

void close_connection(Socket* connection) {
    if (shutdown(connection->file, SHUT_RDWR) < 0) {
        perror("could not close connection");
    }
    close(connection->file);
}

bool wait_get_request(int file, GetRequest* get) {
    IOBuffer_on_stack(buffer, 256);
    Vec line = Vec_with_capacity(128, sizeof(char));

    if (!receive_line(file, &buffer, &line)) {
        Vec_drop(&line);
        return false;
    }
    int sr = sscanf(line.data, "GET %127s HTTP/1.1", &get->path[1]);
    if (sr == EOF || sr != 1) {
        perror("wrong get request format");
        Vec_drop(&line);
        return false;
    }
    get->path[0] = '.';
    get->header = strdup(line.data);

    while (true) {
        Vec_clear(&line);
        if (!receive_line(file, &buffer, &line) ||
            Vec_len(&line) == 1)
        {
            break;
        }
    }

    Vec_drop(&line);
    return true;
}

#define HTTP_HEADER(name, code, message)                                \
    static const char* HTTP_##name##_LINE = "HTTP/1.1 " #code " " message; \
    static const char* HTTP_##name##_CODE = #code
#define HTTP_ERROR(name, code, message)                             \
    HTTP_HEADER(name, code, message);                               \
    static const char* HTTP_##name##_PAGE =                         \
        "<html><body><h1>" #code " " message "</h1></body></html>"
HTTP_HEADER(OK, 200, "OK");
HTTP_ERROR(NOT_FOUND, 404, "Not Found");
HTTP_ERROR(FORBIDDEN, 403, "Forbidden");
HTTP_ERROR(INTERNAL_ERROR, 500, "Internal Server Error");
#undef HTTP_ERROR
#undef HTTP_HEADER

bool receive_line(int file, IOBuffer* buffer, Vec* line) {
    if (read_line(file, buffer, line)) {
        char c;
        while (Vec_pop(line, &c)) {
            if (c != '\r' && c != '\n') {
                Vec_push(line, &c);
                break;
            }
        }
        c = '\0';
        Vec_push(line, &c);
        printf("<< %s\n", (char*)(line->data));
        return true;
    }
    return false;
}

void* answer_get_request(void* request_args) {
    RequestArgs* args = request_args;

    IOBuffer_on_stack(buffer, 512);
    args->entry.time = time(NULL);

#define send_header(args, buffer, name)                              \
    send_line(args->connection->file, buffer, HTTP_##name##_LINE); \
    args->entry.code = HTTP_##name##_CODE

#define send_content_length(args, buffer, len) {                        \
        const size_t LINE_SIZE = 128;                                   \
        char line[LINE_SIZE];                                           \
        size_t cl = len;                                                \
        snprintf(line, LINE_SIZE, "Content-Length: %lu", cl);           \
        send_line(args->connection->file, buffer, line);                \
        args->entry.content_length = cl;                                \
    }

#define send_error(args, buffer, name)                                  \
    sem_wait(&args->can_write);                                         \
    sem_destroy(&args->can_write);                                      \
    send_header(args, buffer, name);                                    \
    send_content_length(args, buffer, strlen(HTTP_##name##_PAGE));      \
    send_line(args->connection->file, buffer, "");                      \
    IOBuffer_write_str(buffer, args->connection->file, HTTP_##name##_PAGE); \
    IOBuffer_flush(buffer, args->connection->file);                     \
    sem_post(args->next_can_write)

    if (!DenialWatcher_check(args->watcher,
                             args->connection->addr.sin_addr.s_addr,
                             args->entry.time)) {
        send_error(args, &buffer, FORBIDDEN);
    } else {
        struct stat st;
        if ((stat(args->request.path, &st) != 0) || (!S_ISREG(st.st_mode))) {
            send_error(args, &buffer, NOT_FOUND);
        } else {
            if ((st.st_mode & S_IXGRP) != 0) {
                int file = execute_out_file(args->request.path);
                if (file < 0) {
                    send_error(args, &buffer, INTERNAL_ERROR);
                } else {
                    send_content(args, &buffer, file);
                }
            } else if ((st.st_mode & S_IRGRP) != 0) {
                int file = open(args->request.path, O_RDONLY);
                if (file < 0) {
                    perror("could not open content file");
                    send_error(args, &buffer, INTERNAL_ERROR);
                } else {
                    send_content(args, &buffer, file);
                }
            } else {
                send_error(args, &buffer, FORBIDDEN);
            }
        }
    }

    args->entry.request = args->request.header;
    Log_append(args->log, &args->entry);
    DenialWatcher_report(args->watcher,
                         args->connection->addr.sin_addr.s_addr,
                         args->entry.time,
                         args->entry.content_length);

    free(args->request.header);
    free(args);
    return NULL;
}

bool send_line(int file, IOBuffer* buffer, const char* line) {
    printf(">> %s\n", line);
    IOBuffer_write_str(buffer, file, line);
    return IOBuffer_write(buffer, file, '\r') &&
        IOBuffer_write(buffer, file, '\n');
}

void send_content(RequestArgs* args,
                  IOBuffer* buffer,
                  int content_file) {
    Vec vec = Vec_with_capacity(128, sizeof(char));
    const char* ext;
    const char* content_type;
    if ((ext = retrieve_extension(args->request.path)) &&
        (content_type = retrieve_mime_type(ext, &vec))) {
    } else {
        content_type = "Content-Type: text/plain";
    }

    sem_wait(&args->can_write);
    sem_destroy(&args->can_write);
    send_header(args, buffer, OK);
    send_content_length(args, buffer, lseek(content_file, 0, SEEK_END));
    lseek(content_file, 0, SEEK_SET);
    const size_t LINE_SIZE = 128;
    char line[LINE_SIZE];
    snprintf(line, LINE_SIZE, "Content-Type: %s", content_type);
    send_line(args->connection->file, buffer, line);

    send_line(args->connection->file, buffer, "");
    IOBuffer_flush(buffer, args->connection->file);

    Vec_drop(&vec);

    printf("-> '%s'\n", args->request.path);
    while (IOBuffer_load(buffer, content_file)) {
        if (!IOBuffer_unload(buffer, args->connection->file)) {
            close(content_file);
            exit(EXIT_FAILURE);
        }
    }

    sem_post(args->next_can_write);
    close(content_file);
}

#undef send_error
#undef send_content_length
#undef send_header

void handle_connection(const Socket* connection,
                       Log* log,
                       DenialWatcher* watcher,
                       int termination) {
    const socklen_t IPSTR_LEN = 128;
    char ipstr[IPSTR_LEN];

    if (inet_ntop(connection->addr.sin_family,
                  &connection->addr.sin_addr,
                  ipstr, IPSTR_LEN) == NULL) {
        perror("could not convert ip address\n");
        strncpy(ipstr, "unkown", IPSTR_LEN);
    }

    uint16_t port = ntohs(connection->addr.sin_port);

    RequestArgs* args = malloc(sizeof(RequestArgs));
    assert_alloc(args);
    sem_init(&args->can_write, 0, 1);

    while (TerminationHandler_wait_read(connection->file, termination)) {
        if (wait_get_request(connection->file, &args->request)) {
            RequestArgs* next_args = malloc(sizeof(RequestArgs));
            assert_alloc(next_args);
            sem_init(&next_args->can_write, 0, 0);

            args->connection = connection;
            args->log = log;
            args->entry = (LogEntry) {
                .address = ipstr,
                .port = port,
                .time = 0,
                .request = "",
                .code = "",
                .content_length = 0,
            };
            args->watcher = watcher;
            args->next_can_write = &next_args->can_write;

            pthread_t t;
            if (pthread_create(&t, NULL, answer_get_request, args) != 0) {
                perror("could not create thread\n");
                sem_destroy(&args->can_write);
                free(args->request.header);
                free(args);
                sem_destroy(&next_args->can_write);
                free(next_args);
                return;
            }
            pthread_detach(t);

            args = next_args;
        }
    }

    sem_wait(&args->can_write);
    sem_destroy(&args->can_write);
    free(args);
}

int execute_out_file(const char* executable) {
    char name[] = "tmpXXXXXX";
    int output = mkstemp(name);
    if (output < 0) {
        perror("could not open temporary output file");
        return -1;
    }
    if (unlink(name) != 0) {
        perror("could not unlink temporary output file");
    }

    pid_t sandbox_pid = fork();
    if (sandbox_pid < 0) {
        perror("could not fork");
        close(output);
        return -1;
    } else if (sandbox_pid == 0) {
        pid_t executable_pid = fork();
        if (executable_pid < 0) {
            perror("could not fork");
            exit(EXIT_FAILURE);
        } else if (executable_pid == 0) {
            if (dup2(output, STDOUT_FILENO) < 0) {
                perror("could not redirect stdout");
                exit(EXIT_FAILURE);
            }
            execlp(executable, executable, NULL);
            perror("could not execute executable");
            exit(EXIT_FAILURE);
        }

        sigset_t mask;
        sigemptyset(&mask);
        struct sigaction action = (struct sigaction) {
            .sa_handler = ignore_sigh,
            .sa_mask = mask,
            .sa_flags = 0,
        };
        if (sigaction(SIGALRM, &action, NULL) != 0) {
            fprintf(stderr, "could not setup SIGARLM action\n");
        }

        alarm(EXECUTION_TIMEOUT_SECONDS);
        int status;
        if (waitpid(executable_pid, &status, 0) != executable_pid) {
            printf("killing executable\n");
            kill(executable_pid, SIGKILL);
        } else if (WIFEXITED(status) && (WEXITSTATUS(status) == EXIT_SUCCESS)) {
            exit(EXIT_SUCCESS);
        }
        exit(EXIT_FAILURE);
    }

    int status;
    bool success = (waitpid(sandbox_pid, &status, 0) == sandbox_pid) &&
        WIFEXITED(status) && (WEXITSTATUS(status) == EXIT_SUCCESS);
    if (success) {
        return output;
    } else {
        close(output);
        return -1;
    }
}

void ignore_sigh(int sig) {
    (void)(sig);
}
