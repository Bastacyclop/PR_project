#include "denial.h"

typedef struct {
    uint32_t address;
    Vec reports;
    time_t kick;
    bool gc_flag;
} Entry;

typedef struct {
    time_t time;
    size_t content_length;
} Report;

static const time_t WATCH_DURATION = 60;
static const time_t KICK_DURATION = 10;
static const unsigned int GC_PERIOD = 5;

static
void drop_entry(void* entry);
static
bool garbage_collect(Entry* entry, time_t time);

bool DenialWatcher_init(DenialWatcher* watcher, size_t threshold) {
    if (pthread_mutex_init(&watcher->mutex, NULL) != 0) {
        fprintf(stderr, "could not initialize mutex\n");
        return false;
    }
    watcher->threshold = threshold;
    watcher->entries = Vec_new(sizeof(Entry));
    return true;
}

void DenialWatcher_drop(DenialWatcher* watcher) {
    if (pthread_mutex_destroy(&watcher->mutex) != 0) {
        fprintf(stderr, "could not destroy mutex\n");
    }
    Vec_drop_with(&watcher->entries, drop_entry);
}

void drop_entry(void* entry) {
    Entry* e = entry;
    Vec_drop(&e->reports);
}

bool DenialWatcher_check(DenialWatcher* watcher,
                         uint32_t address,
                         time_t time) {
    bool check = true;
    pthread_mutex_lock(&watcher->mutex);

    for (size_t e = 0; e < Vec_len(&watcher->entries); e++) {
        Entry* entry = Vec_get_mut(&watcher->entries, e);
        if (entry->address == address) {
            if ((time - entry->kick) <= KICK_DURATION) {
                printf("denial-watcher: kicking again\n");
                entry->kick = time;
                check = false;
            } else {
                size_t total = 0;

                size_t r = 0;
                while (r < Vec_len(&entry->reports)) {
                    Report* report = Vec_get_mut(&entry->reports, r);
                    if ((time - report->time) > WATCH_DURATION) {
                        Vec_swap_remove(&entry->reports, r, NULL);
                        printf("denial-watcher: report collected\n");
                    } else {
                        total += report->content_length;
                        r++;
                    }
                }

                printf("denial-watcher: %lu / %lu\n",
                       total, watcher->threshold);
                if (total > watcher->threshold) {
                    printf("denial-watcher: kicking suspicious address\n");
                    entry->kick = time;
                    check = false;
                }
            }

            entry->gc_flag = false;
            break;
        }
    }

    pthread_mutex_unlock(&watcher->mutex);
    return check;
}

void DenialWatcher_report(DenialWatcher* watcher,
                          uint32_t address,
                          time_t time,
                          size_t content_length) {
    Entry* target = NULL;
    Report report = (Report) {
        .time = time,
        .content_length = content_length,
    };

    pthread_mutex_lock(&watcher->mutex);

    for (size_t e = 0; e < Vec_len(&watcher->entries); e++) {
        Entry* entry = Vec_get_mut(&watcher->entries, e);
        if (entry->address == address) {
            target = entry;
            break;
        }
    }

    if (!target) {
        Entry entry = (Entry) {
            .address = address,
            .reports = Vec_new(sizeof(Report)),
            .kick = 0,
            .gc_flag = false,
        };

        size_t e = Vec_len(&watcher->entries);
        Vec_push(&watcher->entries, &entry);
        target = Vec_get_mut(&watcher->entries, e);
    }

    Vec_push(&target->reports, &report);

    pthread_mutex_unlock(&watcher->mutex);
}

void DenialWatcher_garbage_collect(DenialWatcher* watcher) {
    size_t index = 0;
    // I hope `sleep` is thread-safe, this is not clear
    while (sleep(GC_PERIOD) == 0) {
        if (pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0) {
            fprintf(stderr, "could not set thread cancel state\n");
            break;
        }
        pthread_mutex_lock(&watcher->mutex);

        time_t t = time(NULL);
        // we don't want this value to change with swap removes
        size_t len = Vec_len(&watcher->entries);
        for (size_t i = 0; i < len; i++) {
            Entry* entry = Vec_get_mut(&watcher->entries, index);
            if (entry->gc_flag && garbage_collect(entry, t)) {
                drop_entry(entry);
                Vec_swap_remove(&watcher->entries, index, NULL);
                printf("denial-watcher: entry collected\n");
            } else {
                entry->gc_flag = true;
                index = (index + 1) % Vec_len(&watcher->entries);
            }
        }

        pthread_mutex_unlock(&watcher->mutex);
        if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL) != 0) {
            fprintf(stderr, "could not set thread cancel state\n");
            break;
        }
    }
}

bool garbage_collect(Entry* entry, time_t time) {
    size_t r = 0;
    while (r < Vec_len(&entry->reports)) {
        const Report* report = Vec_get(&entry->reports, r);
        if ((time - report->time) > WATCH_DURATION) {
            Vec_swap_remove(&entry->reports, r, NULL);
            printf("denial-watcher: report collected\n");
        } else {
            r++;
        }
    }

    return Vec_is_empty(&entry->reports);
}
