#include "permits.h"

bool Permits_init(uint32_t max, Permits* p) {
    p->count = 0;
    p->max = max;
    if (pthread_mutex_init(&p->mutex, NULL) != 0) {
        fprintf(stderr, "could not initialize mutex\n");
        return false;
    }
    if (pthread_cond_init(&p->on_release, NULL) != 0) {
        fprintf(stderr, "could not initialize condition\n");
        return false;
    }
    return true;
}

void Permits_acquire(Permits* p) {
    pthread_mutex_lock(&p->mutex);
    while (p->count == p->max) {
        pthread_cond_wait(&p->on_release, &p->mutex);
    }
    (p->count)++;
    pthread_mutex_unlock(&p->mutex);
}

void Permits_release(Permits* p) {
    pthread_mutex_lock(&p->mutex);
    (p->count)--;
    pthread_cond_signal(&p->on_release);
    pthread_mutex_unlock(&p->mutex);
}

void Permits_wait_complete_release(Permits* p) {
    pthread_mutex_lock(&p->mutex);
    while (p->count != 0) {
        pthread_cond_wait(&p->on_release, &p->mutex);
    }
    pthread_mutex_unlock(&p->mutex);
}

void Permits_drop(Permits* p) {
    if (pthread_cond_destroy(&p->on_release) != 0) {
        fprintf(stderr, "could not destroy condition\n");
    }
    if (pthread_mutex_destroy(&p->mutex) != 0) {
        fprintf(stderr, "could not destroy mutex\n");
    }
}
