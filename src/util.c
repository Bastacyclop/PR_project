#include "util.h"
#include <fcntl.h>
#include <ctype.h>

int str_to_int(char* str, int min, int max) {
    assert(str[0] != '\0');
    char* endptr = str;
    long r = strtol(str, &endptr, 10);
    if (endptr[0] != '\0' || r < (long)(min) || r > (long)(max)) {
        printf("expected integer between %i and %i\n", min, max);
        exit(EXIT_FAILURE);
    }
    return (int)(r);
}

bool IOBuffer_load(IOBuffer* buffer, int file) {
    ssize_t r = read(file, buffer->mem, buffer->cap);
    if (r < 0) {
        perror("could not read from file");
        return false;
    } else if (r == 0) {
        return false;
    }

    buffer->pos = 0;
    buffer->len = r;
    return true;
}

bool IOBuffer_unload(IOBuffer* buffer, int file) {
    if (write(file, buffer->mem, buffer->len) != (ssize_t)(buffer->len)) {
        perror("could not write to file");
        return false;
    }

    buffer->pos = 0;
    buffer->len = 0;
    return true;
}

bool IOBuffer_read(IOBuffer* buffer, int file, char* byte) {
    if ((buffer->pos == buffer->len) && !IOBuffer_load(buffer, file)) {
        return false;
    }

    *byte = buffer->mem[buffer->pos];
    (buffer->pos)++;
    return true;
}

bool IOBuffer_write(IOBuffer* buffer, int file, char byte) {
    if ((buffer->len == buffer->cap) && !IOBuffer_unload(buffer, file)) {
        return false;
    }

    buffer->mem[buffer->len] = byte;
    (buffer->len)++;
    return true;
}

bool IOBuffer_flush(IOBuffer* buffer, int file) {
    return (buffer->len == 0) || IOBuffer_unload(buffer, file);
}

void IOBuffer_clear(IOBuffer* buffer) {
    buffer->pos = 0;
    buffer->len = 0;
}

bool IOBuffer_write_str(IOBuffer* buffer, int file, const char* str) {
    for (int i = 0; str[i] != '\0'; i++) {
        if (!IOBuffer_write(buffer, file, str[i])) {
            return false;
        }
    }
    return true;
}

bool read_line(int file, IOBuffer* buffer, Vec* line) {
    char byte;
    while (IOBuffer_read(buffer, file, &byte)) {
        Vec_push(line, &byte);
        if (byte == '\n') { return true; }
    }
    return false;
}

const char* retrieve_extension(const char* path) {
    char* p = strrchr(path, '.');
    if (p) {
        // worst case we return a pointer to `\0`, that's fine
        return &p[1];
    }
    return NULL;
}

size_t whitespace_scan(const char* s, size_t len, size_t* begin, size_t* end) {
    *begin = *end;
    while ((*begin < len) && isspace(s[*begin])) {
        (*begin)++;
    }
    if (*begin == len) {
        return 0;
    }

    *end = *begin;
    while ((*end < len) && !isspace(s[*end])) {
        (*end)++;
    }

    return (*end - *begin);
}

char* retrieve_mime_type(const char* extension, Vec* line) {
    size_t extension_len = strlen(extension);
    int file = open("/etc/mime.types", O_RDONLY);
    if (file < 0) {
        perror("could not open mime.types");
        return NULL;
    }

    IOBuffer_on_stack(buffer, 512);
    while (read_line(file, &buffer, line)) {
        size_t str_len = Vec_len(line) - 1;
        char* str = line->data;
        size_t begin = 0;
        size_t end = 0;
        if (whitespace_scan(str, str_len, &begin, &end) == 0) {
            break;
        }
        size_t type_begin = begin;
        size_t type_end = end;

        size_t len;
        while ((len = whitespace_scan(str, str_len, &begin, &end)) > 0) {
            if (extension_len == len &&
                (strncmp(extension, &str[begin], len) == 0)) {
                str[type_end] = '\0';
                close(file);
                return &str[type_begin];
            }
        }
        Vec_clear(line);
    }

    close(file);
    return NULL;
}
